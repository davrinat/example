package ru.example.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Configuration
public class Config {

    @Bean
    public Connection connection() throws SQLException {
    return DriverManager.getConnection("jdbc:sqlite:database/car_storage.db");
    }
}
