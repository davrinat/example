package ru.example.logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 *Logging class
 */
@Component
@Aspect
public class Logger {

    @Pointcut("execution(* ru.example.service.CarService.*(..))")
    public void CarServicePointCut(){
    }

    @Before("CarServicePointCut()")
    public void beforeMethod(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        List<String> args = getArgs(joinPoint);
        System.out.printf("Method: %s with args: %s started%n", methodName, args);
    }

    @AfterReturning("CarServicePointCut()")
    public void afterMethod(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        List<String> args = getArgs(joinPoint);
        System.out.printf("Method: %s with args: %s finished%n", methodName, args);
    }

    @AfterThrowing(value = "CarServicePointCut()", throwing = "ex")
    public void catchException(JoinPoint joinPoint, Throwable ex) {
        String methodName = joinPoint.getSignature().getName();
        List<String> args = getArgs(joinPoint);
        System.out.printf("ERROR!!! Failed to method = %s with args = %s has error = %s", methodName, args, ex);
    }

    private List<String> getArgs(JoinPoint joinPoint) {
        List<String> args = new ArrayList<>();
        for (int i = 0; i < joinPoint.getArgs().length; i++) {
            Object arg = joinPoint.getArgs()[i];
            args.add("arg" + i + "=" + arg);
        }
        return args;
    }
}
