package ru.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.example.service.CarInfo;
import ru.example.service.CarService;

import java.util.List;

/**
 * RestController
 */
@RestController
@RequestMapping("/car")
public class CarRestController {

    private CarService carService;

    @Autowired
    public void setCarService(CarService carService) {
        this.carService = carService;
    }

    @RequestMapping("/all")
    public List<CarInfo> getAll() {
        return carService.getAll();
    }
}
