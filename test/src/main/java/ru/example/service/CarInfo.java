package ru.example.service;

import org.springframework.stereotype.Component;

import java.time.LocalDate;

/**
 * Entity
 */
@Component
public class CarInfo {

    int id;
    String model;
    String category;
    String description;
    LocalDate manufactureDate;

    public CarInfo() {
    }

    public CarInfo(int id, String model, String category, String description, LocalDate manufactureDate) {
        this.id = id;
        this.model = model;
        this.category = category;
        this.description = description;
        this.manufactureDate = manufactureDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getManufactureDate() {
        return manufactureDate;
    }

    public void setManufactureDate(LocalDate manufactureDate) {
        this.manufactureDate = manufactureDate;
    }
}
