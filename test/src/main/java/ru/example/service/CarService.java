package ru.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.example.dao.CarRepository;

import java.util.List;
import java.util.Objects;

/**
 * Service class
 */
@Component
public class CarService {

    private CarRepository carRepository;

    @Autowired
    public void setCarRepository(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    public List<CarInfo> getAll() {
        String message = "Table does not exist or has been deleted";
        if (Objects.isNull(carRepository.getAll()))
            throw new RuntimeException(message);
        return carRepository.getAll();
    }

    public CarInfo getById(int id) {
        String message = "Car with ID " + id + " not found";
        if (Objects.isNull(carRepository.getById(id)))
            throw new RuntimeException(message);
        return carRepository.getById(id);
    }

    public List<CarInfo> getByModel(String model) {
        String message = "This model " + model + "does not exist in the table";
        if (Objects.isNull(carRepository.getByModel(model)))
            throw new RuntimeException(message);
        return carRepository.getByModel(model);
    }

    public List<CarInfo> getByCategory(String category) {
        String message = "This category " + category + "does not exist in the table";
        if (Objects.isNull(carRepository.getByModel(category)))
            throw new RuntimeException(message);
        return carRepository.getByCategory(category);
    }

    public CarInfo create() {
        return null;
    }

    public CarInfo update() {
        return null;
    }

    public CarInfo delete(int id) {
        String message = "The car with ID " + id + "not found";
        if (Objects.isNull(carRepository.getByModel(String.valueOf(id))))
            throw new RuntimeException(message);
        return carRepository.delete(id);
    }
}
