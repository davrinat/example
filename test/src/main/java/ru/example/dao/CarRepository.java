package ru.example.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.example.service.CarInfo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Class to interact with database
 */
@Component
public class CarRepository {

    private Connection connection;
    private List<CarInfo> cars = new ArrayList<>();

    @Autowired
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    /**
     * getAll from database
     */
    public List<CarInfo> getAll() {
        String sql = "SELECT * FROM car";
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(sql)) {
                while (resultSet.next()) {
                    cars.add(fetchCar(resultSet));
                }
            }
        } catch (Exception e) {
            return null;
        }
        return cars;
    }

    /**
     * Find car by ID
     */
    public CarInfo getById(int id) {
        String sql = "SELECT FROM car WHERE id=?";
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(sql)) {
                return fetchCar(resultSet);
            }
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Find car by model
     */
    public List<CarInfo> getByModel(String model) {
        String sql = "SELECT FROM car WHERE model=?";
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(sql)) {
                while (resultSet.next()) {
                    cars.add(fetchCar(resultSet));
                }
            }
        } catch (Exception ex) {
            return null;
        }
        return cars;
    }

    public List<CarInfo> getByCategory(String category) {
        String sql = "SELECT FROM car WHERE category=?";
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(sql)) {
                while (resultSet.next()) {
                    cars.add(fetchCar(resultSet));
                }
            }
        } catch (Exception ex) {
            return null;
        }
        return cars;
    }

    public CarInfo create(CarInfo info) {
        return null;
    }

    public CarInfo update(CarInfo info) {
        return null;
    }

    public CarInfo delete(int id) {
        String sql = "DELETE FROM car where id=?";
        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(sql)) {
                return fetchCar(resultSet);
            }
        } catch (Exception ex) {
            return null;
        }
    }

    private CarInfo fetchCar(ResultSet resultSet) throws SQLException {
        CarInfo info = new CarInfo();
        info.setId(Integer.parseInt(resultSet.getString("id")));
        info.setModel(resultSet.getString("model"));
        info.setCategory(resultSet.getString("category"));
        info.setDescription(resultSet.getString("description"));
        info.setManufactureDate(LocalDate.parse(resultSet.getString("manufactureDate")));

        return info;
    }
}
